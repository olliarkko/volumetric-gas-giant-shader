////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MIT License
//
//  Copyright (c) 2020 Olli Arkko
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////

Shader "Olli/VolumetricGasGiant"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _VolumTex ("_VolumTexture", 3D) = "white" {}
        _Volum_March_Strength ("_Volum_March_Strength", float) = 1
        _Lighting_Multi ("_Lighting_Multi", float) = 10
        _Density_Loop_Noise_Amount ("_Density_Loop_Noise_Amount", float) = 0.1
        _Lighting_Loop_Noise_Amount ("_Lighting_Loop_Noise_Amount", float) = 0.1
        _Whole_Color ("_Whole_Color", Color) = (1, 1, 1, 1)
        _Gradient_Texture ("_Gradient_Texture", 2D) = "white" {}
        _Time_Factor ("_Time_Factor", float) = 1
        _Highlight_Strength ("_Highlight_Strength", float) = 1
        _Shadow_Color ("_Shadow_Color", Color) = (0, 0, 0, 0)
        _GasGiant_Position ("_GasGiant_Position", Vector) = (0, 0, 0, 0)
        _GasGiant_Radius ("_GasGiant_Radius", float) = 10
        [MaterialToggle] _Render_GasGiant("_Render_GasGiant", Float) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma target 5.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            #define MAX_STEPS 100
            #define SURFACE_DISTANCE 0.001
            #define MAX_DISTANCE 7500
            #define VOLUM_MARCH_STRENGTH 2

            sampler2D _CameraDepthTexture;
            sampler3D _VolumTex;
            float _Volum_March_Strength;
            float _Lighting_Multi;
            float _Density_Loop_Noise_Amount;
            float _Lighting_Loop_Noise_Amount;
            float4 _Whole_Color;
            sampler2D _Gradient_Texture;
            float _Time_Factor;
            float _Highlight_Strength;
            float4 _Shadow_Color;
            float4 _GasGiant_Position;
            float _GasGiant_Radius;
            float _Render_GasGiant;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 viewVector : TEXCOORD1;
            };

            float rand_1_05(in float2 uv)
            {
                float2 noise = (frac(sin(dot(uv ,float2(12.9898,78.233)*2.0)) * 43758.5453));
                return abs(noise.x + noise.y) * 0.5;
            }

            //Use this one for rand
            float nrand(float2 uv)
            {
                return frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453);
            }

            //signed distance function for a sphere
            //TODO: parameterize the center
            float sdSphere(float3 p, float3 center, float rad)
            {
                float3 sphere_pos = center;
                float sphere_rad = rad;
                return length(p - sphere_pos) - sphere_rad;
            }

            //getdist used in normal raymarching, uses the sd functions
            float GetDist(float3 p, float3 center, float rad)
            {
                float dist = sdSphere(p, center, rad);
                return dist;
            }

            
            //this raymarch function is used to find the first hit position
            //of the planet, if no such point is found, then max distance is returned
            float RayMarch(float3 ray_origin, float3 ray_direction, float3 center, float rad)
            {
                float distance_from_origin = 0.0;
                for (int i = 0; i < MAX_STEPS; i++)
                {
                    float3 current_point = ray_origin + distance_from_origin * ray_direction;
                    float distance = GetDist(current_point, center, rad);
                    distance_from_origin += distance;
                    if (distance < SURFACE_DISTANCE || distance_from_origin > MAX_DISTANCE)
                    {
                        return distance_from_origin;
                    }
                }
                return MAX_DISTANCE;
            }

            //this raymarcher is used to find out how long of a distance we should march through the volume
            //in other words it calculates the distance between the initial hit with the sphere and 
            //the exit point (from the sphere) of the ray
            float RayMarchIntersectionDistance(float3 ray_origin, float3 ray_direction, float3 center, float rad)
            {
                float3 current_point = ray_origin + ray_direction * 2 * (rad / 100.0);
                for (int i = 0; i < 200; i++)
                {
                    current_point = current_point + ray_direction * (rad / 100.0);
                    float dist = distance(current_point, center);
                    if (dist >= rad)
                    {
                        current_point = current_point - ray_direction * (dist - rad);
                        break;
                    }
                }
                return distance(ray_origin, current_point);
            }

            float LightingRaymarch(float3 ray_origin, float3 ray_direction)
            {
                float accumulation = 0.0;
                float3 current_point = ray_origin;
                //[unroll(20)]
                [loop]
                for (int i = 0; i < 50; i++)
                {
                    float samp = tex3D(_VolumTex, current_point).r / 50.0;
                    accumulation += samp * _Lighting_Multi;
                    current_point += ray_direction * (0.02 + (nrand(current_point.xy)%0.01)); //(0.01 + (_Lighting_Loop_Noise_Amount * rand_1_05(float2(5.234+ray_origin.x, 21.3+ray_origin.y))));
                    if (accumulation >= 1.0) break;
                    if (distance(current_point, float3(0.5, 0.5, 0.5)) > 0.5) break;
                }
                return clamp((1 - accumulation), 0.0, 1.0);
            }
            float4 LitRayMarch2(float3 ray_origin, float3 ray_direction, float3 center, float rad, float intersect_dist, float3 light_dir)
            {
                //we should first transform the ray origin with the center and rad values
                //to be relative to the -1..1 unit box
                float3 transformed_origin = (ray_origin - center) / (rad);  //(2 * rad)
                //and now since the textures are in 0..1 format for x, y and z...
                float3 final_origin = (transformed_origin + float3(1, 1, 1)) / 2.0;
                //the max distance that we can march inside the volume
                float marching_distance = intersect_dist / (rad);   //(2 * rad)
                //maximum iterations
                int iterations = (int)(marching_distance / 0.0025);
                //and now we are ready to march into the texture
                float distance_from_origin = 0.0 + 0.01; // + (_Density_Loop_Noise_Amount * rand_1_05(float2(5.234+ray_origin.x, 21.3+ray_origin.y)));
                float3 accumulation = float3(0, 0, 0);
                float alpha_acc = 0;
                int count = 0;
                float3 gradient_accumulation = float3(0, 0, 0);
                //[unroll(50)]
                [loop]
                for (int i = 0; i < 200; i++)
                {
                    count++;
                    float3 current_point = final_origin + distance_from_origin * ray_direction;
                    float3 samp = tex3D(_VolumTex, current_point);
                    //accumulation += (samp / 100) * _Volum_March_Strength;   // * (0.1 * (300 - i))
                    float l = LightingRaymarch(current_point, light_dir);
                    accumulation += float3(l, l, l);
                    gradient_accumulation += tex2D(_Gradient_Texture, float2(current_point.x * current_point.z + _Time.y * _Time_Factor, current_point.y)).rgb;
                    alpha_acc += (samp.r / 200) * _Volum_March_Strength;
                    distance_from_origin += 0.005 + (nrand(current_point.xy)%0.0025); // + (_Density_Loop_Noise_Amount * rand_1_05(float2(i-5.234+ray_origin.x, i*21.3+ray_origin.y)));
                    if (distance(current_point, float3(0.5, 0.5, 0.5)) > 0.5) break;
                    if (i >= iterations) break;
                    if (alpha_acc >= 1.0) break;
                }
                //accumulation *= _Whole_Color;
                accumulation *= gradient_accumulation / (((float)count)/_Highlight_Strength);
                return float4(clamp(accumulation.r / (float)count, 0.0, 1.0), clamp(accumulation.g / (float)count, 0.0, 1.0), clamp(accumulation.b / (float)count, 0.0, 1.0), clamp(alpha_acc, 0.0, 1.0));
                //return float4(accumulation, alpha_acc);
            }
            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                float3 viewVector = mul(unity_CameraInvProjection, float4(v.uv * 2 - 1, 0, -1));
				o.viewVector = mul(unity_CameraToWorld, float4(viewVector, 0));
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                float4 col = tex2D(_MainTex, i.uv);
                
                //determine whether to render the gas giant
                if (_Render_GasGiant < 1.0)
                {
                    return col;
                }

                //view direction
                float3 view_dir = normalize(i.viewVector);
                //col.rgb = i.viewVector;

                //depth
                float native_depth = tex2D(_CameraDepthTexture, i.uv).r;
                float linear_depth = LinearEyeDepth(native_depth);
                //col.rgb = linear_depth;

                //camera position
                float3 camera_pos = _WorldSpaceCameraPos;

                //distance to planet
                float dist_to_planet = RayMarch(camera_pos, view_dir, _GasGiant_Position, _GasGiant_Radius);
                //return normal color if the gas giant is behind an object
                if (dist_to_planet > linear_depth) return col;

                //let's calculate the intersection distance
                float intersection_distance = RayMarchIntersectionDistance(camera_pos + view_dir * dist_to_planet, view_dir, _GasGiant_Position, _GasGiant_Radius);
                //intersection distance can't be more than the linear depth
                intersection_distance = min(intersection_distance, linear_depth);

                //light direction
                float3 light_dir = normalize(_WorldSpaceLightPos0.xyz);

                //lit2
                float4 lit_raymarch2 = LitRayMarch2(camera_pos + view_dir * dist_to_planet, view_dir, _GasGiant_Position, _GasGiant_Radius, intersection_distance, light_dir);

                if (dist_to_planet >= MAX_DISTANCE)
                {
                    //just return the normal image color here
                }
                if (dist_to_planet < MAX_DISTANCE)
                {
                    //lit raymarch 2
                    float planet_rgb_amount = lit_raymarch2.a;
                    float original_rgb_amount = 1 - planet_rgb_amount;
                    float3 temp_color = col.rgb * original_rgb_amount + float3(1, 1, 1) * planet_rgb_amount;
                    float original_r_limit = 1.0 - clamp(temp_color.r, 0.0, 1.0);
                    float original_g_limit = 1.0 - clamp(temp_color.g, 0.0, 1.0);
                    float original_b_limit = 1.0 - clamp(temp_color.b, 0.0, 1.0);
                    //col.rgb = col.rgb * original_rgb_amount + lit_raymarch2.rgb * planet_rgb_amount;
                    col.rgb = float3(col.r * original_r_limit, col.g * original_g_limit, col.b * original_b_limit) + lit_raymarch2.rgb * planet_rgb_amount;
                }

                return col;
            }
            ENDCG
        }
    }
}